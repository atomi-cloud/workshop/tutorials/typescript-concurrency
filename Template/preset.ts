export function hi(): void {
	console.log("hi");
}

export function start(): void {
	console.log("start");
}

export function hello(): void {
	console.log("hello");
}

export function bye(): void {
	console.log("bye");
}

export function hey(): void {
	console.log("hey");
}

export function sleep(ms: number): Promise<void> {
	return new Promise<void>(r => setTimeout(r, ms));
}

export function idle(ms: number): void {
	let start = Date.now(), now = start;
	while (now - start < ms) {
		now = Date.now();
	}
}

export function delay(ms: number, callback: Function) {
	setTimeout(callback, ms)
}

